package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	ID string `gorm:"primaryKey"`
	UserName string
	FirstName string
	LastName string
	DateOfBirth int
	PhoneNumber string
	AvatarURL string
}