package controllers

import (
	"github.com/gin-gonic/gin"
	"super-management/api"
	"super-management/api/auth/responses"
	"super-management/api/auth/services"
	authRequests "super-management/api/auth/services/request"
	"super-management/api/base/controller"
)

type AuthenticationController interface {
	 Register(ctx *gin.Context)
	 Login(ctx *gin.Context)
}

type AuthenticationControllerImpl struct {
	controller.BaseController
	jwtService      services.JWTService
	loginService    services.LoginService
	registerService services.RegisterService
}

func (controller *AuthenticationControllerImpl) Prepare() {
	controller.jwtService = api.Provider.GetJWTService()
	controller.loginService = api.Provider.GetLoginService()
	controller.registerService = api.Provider.GetRegisterService()
}

func (controller *AuthenticationControllerImpl) Login(ctx *gin.Context){
	var request authRequests.LoginRequest
	if err := ctx.Bind(&request); err != nil {
		controller.BuildErrorResponse(err, ctx)
	}

	isUserAuthenticated, err := controller.loginService.Login(request.PhoneNumber, request.Password)
	if err != nil {

		controller.BuildErrorResponse("bad request", ctx)
		return
	}

	if isUserAuthenticated {
		token,_ := controller.jwtService.GenerateToken(request.PhoneNumber, true)
		tokenDTO := &responses.AccessTokenResponse{
			AccessToken:  token,
			RefreshToken: token,
			ExpiredIn:    60 * 60 * 24 * 3.,
			TokenType:    "Bearer",
		}
		controller.ServeJson(tokenDTO,ctx)
		return
	}

	controller.BuildErrorResponse("Invalid login info", ctx)
	return
}

func (controller *AuthenticationControllerImpl) Register(ctx *gin.Context) {
	var registerRequest authRequests.RegisterRequest

	if err := ctx.BindJSON(&registerRequest); err != nil {
		controller.BuildErrorResponse(err, ctx)
		return
	}

	user, err := controller.registerService.Register(registerRequest)
	if err != nil {
		controller.BuildErrorResponse(err, ctx)
	}

	token, err := controller.jwtService.GenerateToken(user.PhoneNumber,true)
	if err != nil {
		controller.BuildErrorResponse(err, ctx)
		return
	}

	tokenDTO := &responses.AccessTokenResponse{
		AccessToken:  token,
		RefreshToken: token,
		ExpiredIn:    60 * 60 * 24 * 3,
		TokenType:    "Bearer",
	}
	controller.ServeJson(tokenDTO, ctx)
	return
}