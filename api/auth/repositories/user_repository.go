package repositories

import (
	"gorm.io/gorm"
	"super-management/api/models"
)

type UserRepository interface {
	FindByPhoneNumber(phoneNumber string) (*models.User, error)
	FindById(id string) (*models.User, error)
	Update(user *models.User) (bool, error)
	GetUsers(offset int, limit int) ([] *models.User, error)
	DeleteById(id string) (*models.User, error)
	CreateUser(user models.User) (*models.User, error)
}

type UserRepositoryImpl struct {
	gormDB *gorm.DB
}

func (repository *UserRepositoryImpl) CreateUser(user models.User) (*models.User, error) {
	if err:= repository.gormDB.Create(&user).Error; err != nil {
		return nil, err
	}

	return &user,nil
}

func (repository *UserRepositoryImpl) FindByPhoneNumber(phoneNumber string) (*models.User, error) {
	var user models.User
	if err := repository.gormDB.First(&user, "phone_number = ?", phoneNumber).Error; err != nil {
		return nil, err
	}

	return  &user, nil
}

func (repository *UserRepositoryImpl) DeleteById(id string) (*models.User, error) {
	var user models.User

	if err := repository.gormDB.First(&user, "id = ?", id).Error; err != nil {
		return nil, err
	}

	if err := repository.gormDB.Delete(&user).Error; err != nil {
		return nil,err
	}

	return &user, nil
}

func (repository *UserRepositoryImpl) FindById(id string) (*models.User, error) {
	var user models.User
	if err := repository.gormDB.Find(&user).Error; err != nil {
		return nil,err
	}
	return &user,nil
}

func (repository *UserRepositoryImpl) Update(user *models.User) (bool, error) {
	if err := repository.gormDB.Save(&user).Error; err != nil {
		return false,err
	}
	return true, nil
}

func (repository *UserRepositoryImpl) GetUsers(offset int, limit int) ([]*models.User, error) {

	var users [] *models.User
	if err := repository.gormDB.Offset(offset).Limit(limit).Find(&users).Error; err != nil {
		return nil, err
	}
	return users, nil
}

func NewUserDAO(gormDB *gorm.DB) UserRepository {
	return &UserRepositoryImpl{gormDB: gormDB}
}
