package responses

type UserResponse struct {
	UserName string `json:"user_name"`
	FirstName string `json:"first_name"`
	LastName string `json:"last_name"`
	DateOfBirth int `json:"date_of_birth"`
	PhoneNumber string `json:"phone_number"`
	AvatarURL string `json:"avatar_url"`
}

type AccessTokenResponse struct {
	AccessToken string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
	ExpiredIn int64 `json:"ExpiredIn"`
	TokenType string `json:"TokenType"`
}