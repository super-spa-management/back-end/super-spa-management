package services

import (
	"fmt"
	"super-management/api/auth/repositories"
	"super-management/api/auth/services/request"
	"super-management/api/models"
)

type RegisterService interface {
	Register(request request.RegisterRequest) (*models.User, error)
}

type RegisterServiceImpl struct {
	userDAO repositories.UserRepository
}

func NewRegisterService(userDAO repositories.UserRepository) RegisterService {
	return &RegisterServiceImpl{
		userDAO: userDAO,
	}
}

func (service *RegisterServiceImpl) Register(request request.RegisterRequest) (*models.User, error) {

	user,_ := service.userDAO.FindByPhoneNumber(request.PhoneNumber)
	if user != nil {
		return nil, fmt.Errorf("The User register has been existed")
	}

	user, err := service.userDAO.CreateUser(request.MapToUserModel())
	if err != nil {
		return nil, err
	}

	return user,nil
}