package services

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"os"
	"time"
)

type JWTService interface {
	 GenerateToken(phoneNumber string, isUser bool) (string,error)
	 ValidateToken(encodedToken string) (*jwt.Token, error)
}

type authCustomClaims struct {
	PhoneNumber string `json:"phone_number"`
	IsUser bool `json:"is_user"`
	jwt.StandardClaims
}

type JWTServiceImpl struct {
	secretKey string
}

func NewJWTService() JWTService {
	return &JWTServiceImpl{secretKey: getSecretKey()}
}

func (service *JWTServiceImpl) GenerateToken(phoneNumber string, isUser bool) (string, error) {
	claims := &authCustomClaims{
		PhoneNumber:          phoneNumber,
		IsUser:         false,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(service.secretKey))
	if err != nil {
		return  "",err
	}
	return tokenString, err
}

func (service *JWTServiceImpl)  ValidateToken(encodedToken string) (*jwt.Token, error) {
	return jwt.Parse(encodedToken, func(token *jwt.Token) (interface{}, error) {
		if _, isValid := token.Method.(*jwt.SigningMethodHMAC); !isValid {
			return nil, fmt.Errorf("Invalid token", token.Header["alg"])
		}
		return []byte(service.secretKey), nil
	})
}

func getSecretKey() string {
	secretKey := os.Getenv("secret_key")
	if secretKey == "" {
		return "secret_key"
	}

	return secretKey
}


