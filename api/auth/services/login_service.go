package services

import "os/exec"

type LoginService interface {
	Login(phoneNumber string, password string) (bool,error)
}

func NewLoginService() LoginService {
	return &LoginServiceImpl{}
}

type LoginServiceImpl struct {}

func (service *LoginServiceImpl) Login(phoneNumber string, password string) (bool,error) {
	if phoneNumber == "0896432997" && password == "1anhtuan" {
		return true, nil
	}
	return false, exec.ErrNotFound
}
