package request

type LoginRequest struct {
	PhoneNumber string `form:"phone_number" json:"phone_number"`
	Password string `form:"password" json:"password"`
}