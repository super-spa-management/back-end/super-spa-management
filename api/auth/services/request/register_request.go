package request

import "super-management/api/models"

type RegisterRequest struct {
	UserName string `json:"userName"`
	FirstName string `json:"firstName"`
	LastName string `json:"lastName"`
	 PhoneNumber string `json:"phoneNumber"`
	 Password string `json:"password"`
	 DateOfBirth int `json:"dateOfBirth"`
	 Address string `json:"address"`
}

func (req RegisterRequest) MapToUserModel() models.User {
	return models.User{
		UserName:    req.UserName,
		FirstName:   req.FirstName,
		LastName:    req.LastName,
		DateOfBirth: req.DateOfBirth,
		PhoneNumber: req.PhoneNumber,
		AvatarURL:   "",
	}
}
