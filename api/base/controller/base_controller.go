package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"super-management/api/base/dto"
)

type BaseController struct {}

func (controller * BaseController) BuildErrorResponse( err interface{},ctx *gin.Context) {
	response  := dto.BaseDTO{
		Meta:  dto.MetaDTO{
			Code:    http.StatusBadRequest,
			Message: "Bad Request",
		},
		Data:  nil,
		Error: err,
	}

	ctx.JSON(http.StatusBadRequest, response)
}

func (controller * BaseController) ServeJson(data interface{}, ctx *gin.Context) {
	response := &dto.BaseDTO{
		Meta: dto.MetaDTO{
			Code:    http.StatusOK,
			Message: "Success",
		},
		Data:  data,
		Error: nil,
	}

	ctx.JSON(http.StatusOK, response)
}
