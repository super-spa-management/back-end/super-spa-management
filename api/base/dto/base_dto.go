package dto

type MetaDTO struct {
	 Code int `json:"code"`
	 Message string `json:"message"`
}

type BaseDTO struct {
	Meta MetaDTO `json:"meta"`
	Data interface{} `json:"data"`
	Error interface{} `json:"error"`
}
