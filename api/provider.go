package api

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	//"google.golang.org/api/iterator"
	//"google.golang.org/api/option"
	//"gorm.io/driver/mysql"
	"gorm.io/gorm"
	//"log"
	"super-management/api/auth/repositories"
	"super-management/api/auth/services"
)

const (
	CloudMySqlUserName = "root"
	CloudMySqlPassword = "1anhtuan"
	CloudMySqlProjectId = "noble-return-313803"
	CloudMySqlZone = "us_central1"
	CloudMySqlInstance = "spa-management-421997"
	CloudMySqlDBName = "spa_management"

)

type ServiceProvidable interface {
	GetJWTService() services.JWTService
	GetLoginService() services.LoginService
	GetRegisterService() services.RegisterService
}

type ServiceProvider struct {
	gormMySQLDB *gorm.DB
}

var Provider ServiceProvidable

func InitServiceProvider() ServiceProvidable {
	gormMySQLDB,_  := makeGormMySQLDB()
	return &ServiceProvider{gormMySQLDB: gormMySQLDB}
}

func (provider *ServiceProvider) GetJWTService() services.JWTService {
	return services.NewJWTService()
}

func (provider *ServiceProvider) GetLoginService() services.LoginService {
	return services.NewLoginService()
}

func (provider *ServiceProvider) GetRegisterService() services.RegisterService {
	userDAO := repositories.NewUserDAO(provider.gormMySQLDB)
	return services.NewRegisterService(userDAO)
}

func makeGormMySQLDB() (*gorm.DB, error) {

	//ctx := context.Background()
	//client, err := storage.NewClient(ctx, option.WithCredentialsFile("api/gcloud-cer-key.json"))
	//if err != nil {
	//	fmt.Print("gotted error: ", err)
	//}
	//it := client.Buckets(ctx, CloudMySqlProjectId)
	//for {
	//	battrs, err := it.Next()
	//	if err == iterator.Done {
	//		break
	//	}
	//	if err != nil {
	//		log.Fatal(err)
	//	}
	//	fmt.Println(battrs.Name)
	//}

	//ctx := context.Background()
	//client, err := storage.NewClient(ctx, option.WithCredentialsFile(""))
	//if err != nil {
	//	log.Fatal(err)
	//}
	//fmt.Println("Buckets:")
	//it := client.Buckets(ctx, CloudMySqlProjectId)
	//for {
	//	battrs, err := it.Next()
	//	if err == iterator.Done {
	//		break
	//	}
	//	if err != nil {
	//		log.Fatal(err)
	//	}
	//	fmt.Println(battrs.Name)
	//}
	//dsn := "spa-management-421997:1Anhtuan@unix(/cloudsql/noble-return-313803:us-central1:spa-management-421997-m)/spa_management"

	//db, err := sql.Open("mysql", dsn)
	//if err != nil {
	//	fmt.Print("Got error when parse ", err)
	//}
	//
	//if err := db.Ping(); err != nil {
	//	fmt.Print("Got error when ping ", err)
	//}

	//creds, err := ioutil.ReadFile("api/gcloud-cer-key.json")
	//if err != nil {
	//	fmt.Printf("Read file error %s", err)
	//}
	//
	//cfg, err := goauth.JWTConfigFromJSON(creds, SQLScope)
	//if err != nil {
	//	fmt.Printf("Read file error %s", err)
	//}
	//
	//client := cfg.Client(ctx)
	//proxy.Init(client, nil, nil)
	////
	//cfg := mysql.Cfg("noble-return-313803:us-central1:spa-management-421997",CloudMySqlUserName, CloudMySqlPassword)
	//cfg.DBName = CloudMySqlDBName
	//db, err := mysql.DialCfg(cfg)
	//
	//if err != nil {
	//	fmt.Printf("Gotten error: ", err)
	//}
	//
	//if err := db.Ping(); err != nil {
	//	fmt.Printf("Gotten error: when ping ", err)
	//}


	//mySqlDB, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	dsn := fmt.Sprintf("%s:%s@unix(/%s/%s)/%s?parseTime=true",
		CloudMySqlUserName,
		CloudMySqlPassword,
		"cloudql",
		"noble-return-313803:us-central1:spa-management-421997",
		CloudMySqlDBName)

	db,err := sql.Open("mysql", dsn)
	if err != nil {
		fmt.Print(err)
	}

	err = db.Ping()
	if err != nil {
		fmt.Print(err)
	}
	//
	return nil, nil
}