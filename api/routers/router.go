package routers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"super-management/api"
	"super-management/api/auth/controllers"
)

func init() {

	mainRouter := gin.Default()
	initService()
	authController := controllers.AuthenticationControllerImpl{}
	authController.Prepare()

	authRouterGroup := mainRouter.Group("/super-management/auth")
	{
		authRouterGroup.POST("/login", authController.Login)
		authRouterGroup.POST("/signup", authController.Register)
	}

	if err := mainRouter.Run(); err != nil {
		fmt.Print("Getting error when start router error")
	}
}

func initService() {
	api.Provider =  api.InitServiceProvider()
}

