module super-management

go 1.14

require (
	cloud.google.com/go/storage v1.10.0
	github.com/GoogleCloudPlatform/cloudsql-proxy v1.22.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.1
	github.com/go-playground/validator/v10 v10.6.0 // indirect
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mattn/go-sqlite3 v1.14.7 // indirect
	github.com/spf13/cobra v1.1.3
	github.com/ugorji/go v1.2.5 // indirect
	golang.org/x/crypto v0.0.0-20210506145944-38f3c27a63bf // indirect
	golang.org/x/sys v0.0.0-20210507161434-a76c4d0a0096 // indirect
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/api v0.45.0
	google.golang.org/appengine v1.6.7 // indirect
	gorm.io/driver/mysql v1.0.6
	gorm.io/driver/sqlite v1.1.4 // indirect
	gorm.io/gorm v1.21.9
)
