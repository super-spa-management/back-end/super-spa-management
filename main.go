package main

import (
	_ "super-management/api/routers"
	"super-management/cmd"
)

func main() {

	cmd.Execute()
}
